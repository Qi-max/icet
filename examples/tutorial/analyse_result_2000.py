import os
import pandas as pd
from glob import glob
from mchammer import DataContainer

final_list = list()
ce_directory = 'C:/code/offical_code/icet/examples/tutorial/swapmc_real_1'
ca_directory = 'C:/code/offical_code/icet/examples/tutorial/swapmc_real1'
for temperature in range(0, 1601, 100):
    data_container_file = os.path.join(ce_directory, 'swapmc_step_CanonicalEnsemble_{}k.csv'.format(temperature))
    dc = DataContainer.read(data_container_file)
    final_list.append([temperature, dc.get('potential')[-1]/32, dc.get('mctrial')[-1], len(dc.get('potential')), dc.get('occupations')[-1]])
print(final_list)

data_container_file = os.path.join(ca_directory, 'swapmc_step_CanonicalAnnealing_real_2.csv')
dc = DataContainer.read(data_container_file)
final_list.append(['ce', dc.get('potential')[-1] / 32, dc.get('mctrial')[-1], len(dc.get('potential')), dc.get('occupations')[-1]])
print(final_list)

final_df = pd.DataFrame(final_list, columns=['temperature', 'energy', 'last_accept_id', 'accept_num', 'occupations'])
final_df['accept_rate'] = final_df['accept_num'].apply(lambda x: x/2000)
print(final_df)
final_df.to_csv(os.path.join(ce_directory, 'final_energys_of_diff_temperature.csv'))
