# -*- coding: utf-8 -*-
# This scripts runs in about 2 seconds on an i7-6700K CPU.
import os
import matplotlib.pyplot as plt
from amspectra.utils.plot import set_plot_args_previous, set_plot_args
from icet import ClusterExpansion
import numpy as np
import seaborn as sns
import matplotlib.colors as mcolors

# step 1: Collect ECIs in dictionary
ce = ClusterExpansion.read('mixing_energy_0815_1.ce')
df_ecis = ce.to_dataframe()
df_ecis['ecis'] = df_ecis['eci'] * 1e3

fontsize = 22
# step 2: Plot ECIs
# color = 'lightsteelblue'
fig, axs = plt.subplots(1, 3, sharey=True, figsize=(15, 9))
plt.subplots_adjust(wspace= 0.0001)
set_plot_args_previous(fontsize=fontsize)
# os.makedirs('color')
# for color in mcolors.CSS4_COLORS.keys():
for k, order in enumerate(ce.orders):
    df_order = df_ecis.loc[df_ecis['order'] == order]
    if k < 2 or k > 4:
        continue
    ax = axs[k - 2]
    ax.set_ylim((-2, 4))
    ax.set_xlabel(r'Cluster radius (Å)', fontsize=fontsize, fontfamily='arial')
    if order == 2:
        width = 0.2/3*2
        # color = 'chocolate'
        # color = 'indianred'
        color = 'palevioletred'
        # color = 'mediumvioletred'
        ax.set_xlim((0, 3.5))
        ax.set_ylabel(r'Effective cluster interaction (meV)', fontsize=fontsize, fontfamily='arial')
    if order == 3:
        width = 0.075/3*2
        # color = 'cadetblue'
        # color = 'orange'
        # color = 'mediumpurple'
        # color = 'darkcyan'
        color = 'chocolate'
        ax.set_xlim((1.1, 2.1))
    if order == 4:
        width = 0.075/3*2
        # color = 'brown'
        color = 'steelblue'
        # color = 'lightseagreen'
        # color = 'darkcyan'
        ax.set_xlim((1.1, 2.1))
        # ax.text(0.05, 0.55, 'zerolet: {:.1f} meV'
        #         .format(1e3 * df_ecis.eci.iloc[0]),
        #         transform=ax.transAxes)
        # ax.text(0.05, 0.45, 'singlet: {:.1f} meV'
        #         .format(1e3 * df_ecis.eci.iloc[1]),
        #         transform=ax.transAxes)
    ax.plot([0, 5], [0, 0], color='darkgray', ls='--', lw=1)

    ax.bar(list(df_order.radius), 1e3 * np.array(list(df_order.eci)),
           width=width, color=color, alpha=1, zorder=0)
    # sns.catplot(x="radius", y="ecis", kind="bar", data=df_ecis)
    ax.scatter(list(df_order.radius), 1e3 * np.array(list(df_order.eci)),
               s=25, color='black', zorder=10, marker='x', linewidths=1)

    # ax.scatter(df_order.radius, len(df_order) * [-0.7],
    #            marker='o', s=2.0)
    ax.text(0.05, 0.94, 'order: {}'.format(order),
            transform=ax.transAxes)
    # ax.text(0.05, 0.81, '#parameters: {}'.format(len(df_order)),
    #         transform=ax.transAxes,)
    # ax.text(0.05, 0.71, '#non-zero params: {}'
    #         .format(np.count_nonzero(df_order.eci)),
    #         transform=ax.transAxes,)
    l_wid = 1.5
    # set_plot_args(ax, l_wid=2, aspect=False, fontsize=15, yticks=None)
    ax.tick_params(labelsize=fontsize - 2)
    # ax.spines['left'].set_visible(plot_args.get('left_visible', True))
    ax.spines['left'].set_linewidth(l_wid)

    # ax.spines['bottom'].set_visible(plot_args.get('bottom_visible', True))
    ax.spines['bottom'].set_linewidth(l_wid)

    # ax.spines['right'].set_visible(plot_args.get('right_visible', True))
    ax.spines['right'].set_linewidth(l_wid)

    # ax.spines['top'].set_visible(plot_args.get('top_visible', True))
    ax.spines['top'].set_linewidth(l_wid)
    # ax.spines.set_linewidth(2)
    ax.tick_params(which='major', width=1)
    ax.tick_params(which='major', length=5)
    ax.tick_params(which='minor', width=0.7)
    ax.tick_params(which='minor', length=2.5)

plt.savefig('ecis7.png'.format(color), ppi=300, bbox_inches='tight')
