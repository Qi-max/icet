import os

from amlearn.utils.check import check_output_path
from mchammer import DataContainer


n_steps = 200000
temperature = 900
input_path = 'C:/code/offical_code/icet/examples/tutorial/swapmc_real_112_122/steps_{}'.format(n_steps)
output_path = r'D:\dataset\HEA\cluster_expansion\{}\{}'.format(n_steps, temperature)
check_output_path(output_path)
dc = DataContainer.read(os.path.join(input_path,
                                     'swapmc_step{}_CanonicalEnsemble_{}k.csv'.format(n_steps, temperature)))

from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.io.vasp import Poscar

for mctrial, occupations in zip(dc.get('mctrial'), dc.get('occupations')):
    poscar = Poscar(AseAtomsAdaptor.get_structure(occupations),
                    sort_structure=True)
    poscar.write_file(os.path.join(output_path, 'POSCAR_{}_step{}'.format(temperature, mctrial)))

