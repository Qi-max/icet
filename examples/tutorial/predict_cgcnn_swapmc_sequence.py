# This scripts runs in about 16 seconds on an i7-6700K CPU.
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from ase.io.vasp import read_vasp
import seaborn as sns
from amlearn_private.utils.ml import combine_score_regression
from icet import ClusterExpansion


sequence_dict = {'v1_2_2_rs3964': 'p1_rs3964_v2',
                 'v1_2_3_rs9587': 'p1_rs9587_v3',
                 'v1_2_4_rs9189919': 'p1_rs9189919_v4',}
# sequence_key = 'v1_2_2_rs3964'
# sequence_key = 'v1_2_3_rs9587'
sequence_key = 'v1_2_4_rs9189919'
sequence_path = r'D:\dataset\HEA\swap_mc_cgcnn_original\VCoNi\v1_4_1_nc3_ns14_136\{}'.format(sequence_dict[sequence_key])
ce = ClusterExpansion.read(r'C:/code/offical_code/icet/examples/tutorial/mixing_energy_0815_1.ce')

result_list = list()
for file in sorted(os.listdir(sequence_path), key=lambda x: int(x.split('_')[1].replace('swap', '')) if '_' in x else 10000000):
    if not file.startswith('POSCAR'):
        continue
    swap_id = int(file.split('_')[1].replace('swap', ''))
    structure = read_vasp(os.path.join(sequence_path, file))

    # the factor of 1e3 serves to convert from eV/atom to meV/atom
    cgcnn_energy = float(file.split('_')[-1].replace('tf', ''))
    predicted_energy = ce.predict(structure) * 108
    result_list.append([swap_id, cgcnn_energy, predicted_energy])

result_df = pd.DataFrame(result_list, columns=['swap_id', 'cgcnn_energy', 'predicted_energy'])
os.makedirs(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence', exist_ok=True)
# result_df.to_csv(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence/predict_{}_sequence.csv'.format(sequence_key))
print(combine_score_regression(result_df['cgcnn_energy'], result_df['predicted_energy']))

sns.set(style="darkgrid")
g = sns.relplot(x="swap_id", y="predicted_energy", kind="line", data=result_df)
# plt.show()
plt.savefig(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence/predict_{}_sequence.png'.format(sequence_key),
            dpi=300, bbox_inches='tight')
plt.close()

dft_file = r'D:\dataset\HEA\VCoNi\VCoNi_ACTv1_{}.gz'.format(sequence_key)
dft_df = pd.read_pickle(dft_file)
dft_df = dft_df[np.array([x.split("_")[-1] for x in dft_df.index]) == "1"]
dft_df['group_id'] = dft_df.index
dft_df.index = dft_df['group_id'].apply(lambda x: int(x.split('_')[6].replace('swap', '')))
dft_df['swap_id'] = dft_df.index

result_df.index = result_df['swap_id']
result_df['energy'] = dft_df['energy']
result_df.to_csv(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence/predict_{}_sequence.csv'.format(sequence_key))

dft_df['cgcnn_energy'] = result_df['cgcnn_energy']
dft_df['predicted_energy'] = result_df['predicted_energy']
dft_df.to_csv(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence/dft_predict_by_ClusterExpansion_CGCNN_{}.csv'.format(sequence_key))

melt_df = pd.melt(dft_df, id_vars=['swap_id'], value_vars=['energy', 'cgcnn_energy', 'predicted_energy'])
sns.relplot(x="swap_id", y="value", hue="variable",
            dashes=True, markers=True, kind="line", data=melt_df)
# plt.show()
plt.savefig(r'C:/code/offical_code/icet/examples/tutorial/predict_cgcnn_sequence/dft_predict_by_ClusterExpansion_CGCNN_{}.png'.format(sequence_key),
            dpi=300, bbox_inches='tight')
plt.close()
