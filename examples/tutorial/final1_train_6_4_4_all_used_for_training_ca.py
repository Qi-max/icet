# This scripts runs in about 16 seconds on an i7-6700K CPU.
import os
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from ase.io.vasp import read_vasp
from pymatgen.io.ase import AseAtomsAdaptor
from icet import (ClusterSpace, StructureContainer,
                  CrossValidationEstimator, ClusterExpansion)
from sklearn.model_selection import train_test_split
from amlearn_private.utils.ml import combine_score_regression

from ase.build import make_supercell
from icet import ClusterExpansion
from mchammer.calculators import ClusterExpansionCalculator
from mchammer.ensembles import SemiGrandCanonicalEnsemble, CanonicalAnnealing, CanonicalEnsemble
import numpy as np
from os import mkdir

from mchammer.data_containers.base_data_container import BaseDataContainer

file_path = r'D:\dataset\HEA\VCoNi'
train_file_list = [
                   'RG_structures_energies_forces_resurvey_marcc_50_71.gz',
                   'RG_structures_energies_forces_resurvey_marcc_try2.gz',
                   'VCoNi_ACTv1_v1_1_1_nc3_ns14_136_rs52.gz',
                   # 'RG_structures_energies_forces_resurvey_marcc_try3.gz',
                   # 'RG_structures_energies_forces_resurvey_marcc.gz',
                   ]
test_file_list = [
                  # 'VCoNi_ACTv1_v1_1_1_nc3_ns14_136_rs52.gz',
                  'VCoNi_ACTv1_v1_2_2_rs3964.gz',
                  'VCoNi_ACTv1_v1_2_3_rs9587.gz',
                  'VCoNi_ACTv1_v1_2_4_rs9189919.gz'
                  ]

df = pd.read_pickle(os.path.join(file_path, r"VCoNi_structures_energies_forces_all.pickle.gz"))
ids = df["group_id"].apply(lambda x: x.split("_")[-1])
df = df[ids == "1"]

if train_file_list:
    for train_file in train_file_list:
        train_df = pd.read_pickle(os.path.join(file_path, train_file))
        ids = np.array([x.split("_")[-1] for x in train_df.index])
        train_df = train_df[ids == "1"]
        train_df['group_id'] = train_df.index
        df = pd.concat([df, train_df], ignore_index=True)
df['energy'] = df['energy']/108
print(df)

train_df = df
train_idx = train_df.index
train_df["energy"].hist()
#
#
all_test_df = pd.read_pickle(os.path.join(file_path, test_file_list[0]))
ids = np.array([x.split("_")[-1] for x in all_test_df.index])
all_test_df = all_test_df[ids == "1"]

if test_file_list[1:]:
    for test_file in test_file_list[1:]:
        test_df = pd.read_pickle(os.path.join(file_path, test_file))
        ids = np.array([x.split("_")[-1] for x in test_df.index])
        test_df = test_df[ids == "1"]
        all_test_df = pd.concat([all_test_df, test_df], ignore_index=True)

test_idx = all_test_df.index
all_test_df["energy"] = all_test_df["energy"]/108
all_test_df["energy"].hist()
plt.show()
#
primitive_structure = AseAtomsAdaptor.get_atoms(train_df['structure'].iloc[0])
cs = ClusterSpace(structure=primitive_structure,
                  cutoffs=[6.0, 4.0, 4.0],
                  chemical_symbols=['V', 'Co', 'Ni'])
print(cs)

# step 3: Parse the input structures and set up a structure container
sc = StructureContainer(cluster_space=cs)
for idx, row in train_df.iterrows():
    sc.add_structure(structure=AseAtomsAdaptor.get_atoms(row['structure']),
                     properties={'mixing_energy': row['energy']})
print(sc)

print("sc.get_fit_data(key='mixing_energy'): \n{}".format(sc.get_fit_data(key='mixing_energy')))

# for lam in threshold_lambdas:
#     cve = CrossValidationEstimator((X, y), fit_method='ardr', validation_method='shuffle-split',
#                                    threshold_lambda=lam, test_size=0.1, train_size=0.9,
#                                    n_splits=cv_splits, **kwargs)

# step 4: Train parameters
opt = CrossValidationEstimator(fit_data=sc.get_fit_data(key='mixing_energy'),
                               fit_method='lasso', standardize=False)
opt.validate()
opt.train()
print(opt)
#
ce = ClusterExpansion(cluster_space=cs, parameters=opt.parameters, metadata=opt.summary)
print(ce)

# test
data = {'test_idx': [], 'reference_energy': [], 'predicted_energy': []}
for idx, row in all_test_df.iterrows():
    data['test_idx'].append(idx)
    # the factor of 1e3 serves to convert from eV/atom to meV/atom
    data['reference_energy'].append(row['energy'])
    data['predicted_energy'].append(ce.predict(AseAtomsAdaptor.get_atoms(row['structure'])))
print(combine_score_regression(data['reference_energy'], data['predicted_energy']))

# step 2: Plot results
fig, ax = plt.subplots(figsize=(4, 3))
ax.scatter(data['reference_energy'], data['predicted_energy'],
           marker='x', label='CE prediction')
plt.savefig('prediction_energy_comparison_{}.png'.format(datetime.datetime.now().strftime(
                             '%Y-%m-%d_%H-%M-%S')), bbox_inches='tight')


# ce.write('./mixing_energy_0816_ardr_2.ce')
ce.write('./mixing_energy_0816_lasso_3train.ce')
#
#
# # step 1: Set up structure to simulate as well as calculator
# ce = ClusterExpansion.read(r'C:/code/offical_code/icet/examples/tutorial/mixing_energy_0815_1.ce')
# # structure = make_supercell(ce.get_cluster_space_copy().primitive_structure,
# #                            3 * np.array([[-1, 1, 1],
# #                                          [1, -1, 1],
# #                                          [1, 1, -1]]))
# structure = make_supercell(primitive_structure,
#                            2 * np.array([[-1, 1, 1],
#                                          [1, -1, 1],
#                                          [1, 1, -1]]))
# # structure = read_vasp(r"D:\dataset\HEA\VCoNi\POSCAR_ACCEPT_0p000_step_6.6")
# calculator = ClusterExpansionCalculator(structure, ce)
#
# n_steps = 10000
#
# # step 2: Carry out Monte Carlo simulations
# # Make sure output directory exists
# output_directory = 'C:/code/offical_code/icet/examples/tutorial/swapmc_real/steps_{}'.format(n_steps)
# os.makedirs(output_directory, exist_ok=True)
# # for temperature in [900, 300]:
# #     # Evolve configuration through the entire composition range
# # for dmu in np.arange(-0.7, 0.51, 0.05):
# # Initialize MC ensemble
# mc = CanonicalAnnealing(
#     structure=structure, calculator=calculator,
#     T_start=1600, T_stop=0, n_steps=n_steps,
#     user_tag='test-ensemble', random_seed=19921202,
#     dc_filename=os.path.join(output_directory, 'swapmc_step{}_CanonicalAnnealing.csv'.format(n_steps)),
#     data_container_write_period=1.0,
#     ensemble_data_write_interval=2,
#     trajectory_write_interval=4
# )
# mc.run()
#
# #
# # for temperature in range(0, 1601, 100):
# #     mc = CanonicalEnsemble(
# #         structure=structure, calculator=calculator,
# #         temperature=temperature, user_tag='test-ensemble', random_seed=19921202,
# #         dc_filename=os.path.join(output_directory,
# #                                  'swapmc_step_CanonicalEnsemble_{}k.csv'.format(temperature)),
# #         data_container_write_period=1.0,
# #         ensemble_data_write_interval=2,
# #         trajectory_write_interval=4
# #     )
# #     #
# #     # from mchammer.observers import ClusterExpansionObserver as CEObserver
# #     # # step 5: Attach observer and run
# #     # observer = CEObserver(cluster_expansion=ce, interval=10)
# #     # mc.attach_observer(observer=observer, tag='lattice_parameter')
# #     mc.run(number_of_trial_steps=10000)
# #
# #     # step 6: Print data
#     # print(mc.data_container.data)
#     #
#     # # mc.run()
#     # structure = mc.structure
#     # #
#     # # import pandas as pd
#     # # from glob import glob
#     # from mchammer import DataContainer
#     # #
#     # filename = os.path.join(output_directory, 'swapmc_step2.csv')
#     # dc = DataContainer.read(filename)
#     # data_row = dc.ensemble_parameters
#     # data_row['filename'] = filename
#     # n_atoms = data_row['n_atoms']
#     # #
#     # equilibration = 5 * n_atoms
#     #
#     # stats = dc.analyze_data('Pd_count', start=equilibration)
#     # data_row['Pd_concentration'] = stats['mean'] / n_atoms
#     # data_row['Pd_concentration_error'] = stats['error_estimate'] / n_atoms
#     #
#     # stats = dc.analyze_data('potential', start=equilibration)
#     # data_row['mixing_energy'] = stats['mean'] / n_atoms
#     # data_row['mixing_energy_error'] = stats['error_estimate'] / n_atoms
#     #
#     # data_row['acceptance_ratio'] = \
#     #     dc.get_average('acceptance_ratio', start=equilibration)
#     # if ensemble == 'sgc':
#     #     data_row['free_energy_derivative'] = \
#     #         dc.ensemble_parameters['mu_Pd'] - \
#     #         dc.ensemble_parameters['mu_Ag']
#     # elif ensemble == 'vcsgc':
#     #     data_row['free_energy_derivative'] = \
#     #         dc.get_average('free_energy_derivative_Pd', start=equilibration)
#     #
#     # data.append(data_row)
#     #
#     # # step 2: Write data to pandas dataframe in csv format
#     # df = pd.DataFrame(data)
#     # df.to_csv('monte-carlo-{}.csv'.format(ensemble), sep='\t')
#     #
#     # #
#     # # # step 5: Construct cluster expansion and write it to file
#     # # ce = ClusterExpansion(cluster_space=cs, parameters=opt.parameters, metadata=opt.summary)
#     # # print(ce)
#     # # ce.write('./mixing_energy.ce')