import os
import pandas as pd
from glob import glob
from mchammer import DataContainer
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.io.vasp import Poscar
import matplotlib.pyplot as plt
import seaborn as sns

final_list = list()
n_steps = 10000
ca_directory = 'C:/code/offical_code/icet/examples/tutorial/swapmc_real/108_atoms_steps_{}'.format(n_steps)
sns.set(style="darkgrid")

for temperature in range(0, 601, 100):
    data_container_file = os.path.join(ca_directory, 'swapmc_step{}_CanonicalEnsemble_{}k.csv'.format(n_steps, temperature))
    dc = DataContainer.read(data_container_file)

    df = pd.DataFrame(list(zip(dc.get('mctrial'), dc.get('potential'))), columns=['swap_id', 'energy'])
    g = sns.relplot(x='swap_id', y='energy', kind="line", data=df)
    # plt.show()
    plt.savefig(os.path.join(ca_directory, 'swapmc_step{}_CanonicalEnsemble_{}k.png'.format(n_steps, temperature)),
                dpi=300, bbox_inches='tight')
    plt.close()
    poscar = Poscar(AseAtomsAdaptor.get_structure(dc.get('occupations')[-1]),
                    sort_structure=True)
    poscar.write_file(os.path.join(ca_directory, 'POSCAR_{}k_step{}'.format(temperature, dc.get('mctrial')[-1])))

    final_list.append([temperature, dc.get('potential')[-1], dc.get('mctrial')[-1], len(dc.get('potential')), dc.get('occupations')[-1]])
print(final_list)
#
# data_container_file = os.path.join(ca_directory, 'swapmc_step{}_CanonicalAnnealing.csv'.format(n_steps, 'ca'))
# dc = DataContainer.read(data_container_file)
# df = pd.DataFrame(list(zip(dc.get('mctrial'), dc.get('potential') / 32)), columns=['swap_id', 'energy'])
# g = sns.relplot(x='swap_id', y='energy', kind="line", data=df)
# # plt.show()
# plt.savefig(os.path.join(ca_directory, 'swapmc_step{}_CanonicalAnnealing.png'.format(n_steps, 'ca')),
#             dpi=300, bbox_inches='tight')
# plt.close()
# poscar = Poscar(AseAtomsAdaptor.get_structure(dc.get('occupations')[-1]),
#                 sort_structure=True)
# poscar.write_file(os.path.join(ca_directory, 'POSCAR_{}_step{}'.format('ca', dc.get('mctrial')[-1])))
# final_list.append(['ce', dc.get('potential')[-1] / 32, dc.get('mctrial')[-1], len(dc.get('potential')), dc.get('occupations')[-1]])
# print(final_list)

final_df = pd.DataFrame(final_list, columns=['temperature', 'energy', 'last_accept_id', 'accept_num', 'occupations'])
final_df['accept_rate'] = final_df['accept_num'].apply(lambda x: x/2000)
print(final_df)
final_df.to_csv(os.path.join(ca_directory, 'final_energys_of_diff_temperature_step{}.csv'.format(n_steps)))


