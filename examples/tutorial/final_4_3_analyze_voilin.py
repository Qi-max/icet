# -*- coding: utf-8 -*-
# This scripts runs in about 2 seconds on an i7-6700K CPU.
import os
import matplotlib.pyplot as plt
from icet import ClusterExpansion
import numpy as np
import seaborn as sns
import matplotlib.colors as mcolors

# step 1: Collect ECIs in dictionary
ce = ClusterExpansion.read('mixing_energy_0815_1.ce')
df_ecis = ce.to_dataframe()
df_ecis['ecis'] = df_ecis['eci'] * 1e3

# step 2: Plot ECIs
# color = 'lightsteelblue'
fig, axs = plt.subplots(1, 3, sharey=True, figsize=(7.5, 3))
plt.subplots_adjust(wspace= 0.0001)

# os.makedirs('color')
# for color in mcolors.CSS4_COLORS.keys():
for k, order in enumerate(ce.orders):
    df_order = df_ecis.loc[df_ecis['order'] == order]
    if k < 2 or k > 4:
        continue
    ax = axs[k - 2]
    ax.set_ylim((-2, 5))
    ax.set_xlabel(r'Cluster radius (Å)')
    if order == 2:
        width = 0.2
        color = 'chocolate'
        ax.set_xlim((0, 4.0))
        ax.set_ylabel(r'Effective cluster interaction (meV)')
    if order == 3:
        width = 0.075
        color = 'cadetblue'
        ax.set_xlim((1, 2.5))
    if order == 4:
        width = 0.075
        color = 'darkcyan'
        ax.set_xlim((1, 2.5))
        # ax.text(0.05, 0.55, 'zerolet: {:.1f} meV'
        #         .format(1e3 * df_ecis.eci.iloc[0]),
        #         transform=ax.transAxes)
        # ax.text(0.05, 0.45, 'singlet: {:.1f} meV'
        #         .format(1e3 * df_ecis.eci.iloc[1]),
        #         transform=ax.transAxes)
    ax.plot([0, 5], [0, 0], color='darkgray', ls='--', lw=1)
    # ax.bar(list(df_order.radius), 1e3 * np.array(list(df_order.eci)),
    #        width=width, color=color, alpha=0.2)
    # sns.catplot(x="radius", y="ecis", kind="bar", data=df_ecis)
    # ax.scatter(list(df_order.radius), 1e3 * np.array(list(df_order.eci)), s=4)
    # sns.violinplot(x="radius", y="ecis", data=df_order)
    # sns.catplot(x="radius", y="ecis", jitter=False, data=df_order, ax=ax)
    positions = list()
    data_list = list()
    for eci, radius in zip(1e3 * np.array(list(df_order.eci)), list(df_order.radius)):
        if not positions or abs(positions[-1] - radius) > 0.00002:
            positions.append(radius)
            data_list.append([eci])
        else:
            data_list[-1].append(eci)

    ax.violinplot(data_list, positions=positions,
                  showmeans=False, showmedians=True)
    #
    # positions = list()
    # data_list = list()
    # for eci, radius in zip(1e3 * np.array(list(df_order.eci)), list(df_order.radius)):
    #     if radius not in positions:
    #         positions.append(radius)
    #         data_list.append([eci])
    #     else:
    #         data_list[-1].append(eci)
    #
    # ax.violinplot(data_list, positions=positions,
    #               showmeans=False, showmedians=True)

    # ax.scatter(df_order.radius, len(df_order) * [-0.7],
    #            marker='o', s=2.0)
    ax.text(0.05, 0.91, 'order: {}'.format(order),
            transform=ax.transAxes)
    # ax.text(0.05, 0.81, '#parameters: {}'.format(len(df_order)),
    #         transform=ax.transAxes,)
    # ax.text(0.05, 0.71, '#non-zero params: {}'
    #         .format(np.count_nonzero(df_order.eci)),
    #         transform=ax.transAxes,)

plt.savefig('ecis6.png'.format(color), bbox_inches='tight')
